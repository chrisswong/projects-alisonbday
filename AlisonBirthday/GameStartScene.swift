//
//  GameStartScene.swift
//  AlisonBirthday
//
//  Created by Poon Ka Hang on 8/2/2015.
//  Copyright (c) 2015年 Green Tomato. All rights reserved.
//

import SpriteKit

class GameStartScene: SKScene {
    override func didMoveToView(view: SKView) {
        backgroundColor = SKColor.whiteColor()
        
        let bgImage = SKSpriteNode(imageNamed:"gameplay_bg")
        bgImage.size = self.size
        bgImage.position = CGPointMake(self.size.width/2, self.size.height/2)
        addChild(bgImage)
        
        let label = SKLabelNode(fontNamed: "Chalkduster")
        label.text = "Start"
        label.fontSize = 40
        label.fontColor = SKColor.blackColor()
        label.position = CGPoint(x: size.width/2, y: size.height/2)
        label.name = "start"
        addChild(label)
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        // 1 - Choose one of the touches to work with
        let touch = touches.anyObject() as UITouch
        let touchLocation = touch.locationInNode(self.scene)
        let node = self.nodeAtPoint(touchLocation)
        
        if (node.name == "start") {
            let reveal = SKTransition.fadeWithDuration(0.5)
            let gameScene = GameScene(size: self.size)
            self.view?.presentScene(gameScene, transition: reveal)
        }
    }
}