//
//  Player.swift
//  AlisonBirthday
//
//  Created by Poon Ka Hang on 4/2/2015.
//  Copyright (c) 2015年 Green Tomato. All rights reserved.
//
import SpriteKit

class Player: SKSpriteNode {
    
    override init() {
        let texture = SKTexture(imageNamed: "alison_player")
        super.init(texture: texture, color: nil, size: texture.size())
        self.physicsBody = SKPhysicsBody(texture: texture, size: texture.size())
        self.physicsBody?.dynamic = true
        self.physicsBody?.allowsRotation = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.Player
        self.physicsBody?.contactTestBitMask = PhysicsCategory.All
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func moveToLeft(){
        let actionMoveLeft:SKAction = SKAction.moveByX(-100,y: 0,duration:0.5)
        let actionForever:SKAction = SKAction.repeatActionForever(actionMoveLeft)
        self.runAction(actionForever)
    }
    
    func moveToRight(){
        let actionMoveRight:SKAction = SKAction.moveByX(100,y: 0,duration:0.5)
        let actionForever:SKAction = SKAction.repeatActionForever(actionMoveRight)
        self.runAction(actionForever)
    }
    
    func stop(){
        self.removeAllActions();
    }
    
}