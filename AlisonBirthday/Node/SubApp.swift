//
//  AXAApp.swift
//  AlisonBirthday
//
//  Created by Poon Ka Hang on 3/2/2015.
//  Copyright (c) 2015年 Green Tomato. All rights reserved.
//
import SpriteKit

class AXAApp: App {
    override init() {
        let texture = SKTexture(imageNamed: "appicon3")
        super.init(texture: texture, color: nil, size: texture.size())
        self.score = 3
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PizzaHutApp: App {
    override init() {
        let texture = SKTexture(imageNamed: "appicon1")
        super.init(texture: texture, color: nil, size: texture.size())
        self.score = 3
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ColaApp: App {
    override init() {
        let texture = SKTexture(imageNamed: "appicon4")
        super.init(texture: texture, color: nil, size: texture.size())
        self.score = 3
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class KielhsApp: App {
    override init() {
        let texture = SKTexture(imageNamed: "appicon2")
        super.init(texture: texture, color: nil, size: texture.size())
        self.score = 3
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class GrandTagApp: App {
    override init() {
        let texture = SKTexture(imageNamed: "appicon5")
        super.init(texture: texture, color: nil, size: texture.size())
        self.score = 3
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class OctopusApp: App {
    override init() {
        let texture = SKTexture(imageNamed: "appicon6")
        super.init(texture: texture, color: nil, size: texture.size())
        self.score = 3
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SCMPApp: App {
    override init() {
        let texture = SKTexture(imageNamed: "appicon7")
        super.init(texture: texture, color: nil, size: texture.size())
        self.score = 3
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class Cake: App {
    override init() {
        let texture = SKTexture(imageNamed: "cake")
        super.init(texture: texture, color: nil, size: texture.size())
        self.score = 10
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
