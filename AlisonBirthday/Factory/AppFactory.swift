//
//  AppFactory.swift
//  AlisonBirthday
//
//  Created by Poon Ka Hang on 4/2/2015.
//  Copyright (c) 2015年 Green Tomato. All rights reserved.
//

import Foundation

class AppFactory : NSObject{

    class var sharedInstance : AppFactory {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : AppFactory? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = AppFactory()
        }
        return Static.instance!
    }
    
    func createApp() -> App {
        var random = Int(arc4random_uniform(20))
        var app:App
        
        switch random{
        case 0:
            app = PizzaHutApp()
            break
        case 1:
            app = KielhsApp()
            break
        case 2:
            app = AXAApp()
            break
        case 3:
            app = ColaApp()
            break
        case 4:
            app = GrandTagApp()
            break
        case 5:
            app = OctopusApp()
            break
        case 6:
            app = SCMPApp()
            break
        case 7:
            app = Shitapp()
            break
        case 8,9,10:
            app = Cake()
            break
        default:
            app = Shitapp()
            break
        }
        return app
    }
}