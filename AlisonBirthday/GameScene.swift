//
//  GameScene.swift
//  AlisonBirthday
//
//  Created by Poon Ka Hang on 3/2/2015.
//  Copyright (c) 2015年 Green Tomato. All rights reserved.
//

import SpriteKit
import AVFoundation

struct PhysicsCategory {
    static let None      : UInt32 = 0
    static let All       : UInt32 = UInt32.max
    static let App   : UInt32 = 0b1       // 1
    static let Player: UInt32 = 0b10      // 2
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let player = Player()
    let score = SKLabelNode()
    let life1 = SKSpriteNode(texture:SKTexture(imageNamed: "heart-1"),color: nil, size:CGSize(width: 20, height: 20))
    let life2 = SKSpriteNode(texture:SKTexture(imageNamed: "heart-1"),color: nil, size:CGSize(width: 20, height: 20))
    let life3 = SKSpriteNode(texture:SKTexture(imageNamed: "heart-1"),color: nil, size:CGSize(width: 20, height: 20))
    // This positions the label node in the scene
    var backgroundMusicPlayer: AVAudioPlayer!
    
    func playBackgroundMusic(filename: String) {
        let url = NSBundle.mainBundle().URLForResource(
            filename, withExtension: nil)
        if (url == nil) {
            println("Could not find file: \(filename)")
            return
        }
        
        var error: NSError? = nil
        backgroundMusicPlayer =
            AVAudioPlayer(contentsOfURL: url, error: &error)
        if backgroundMusicPlayer == nil {
            println("Could not create audio player: \(error!)")
            return
        }
        
        backgroundMusicPlayer.numberOfLoops = -1
        backgroundMusicPlayer.prepareToPlay()
        backgroundMusicPlayer.play()
    }
    
    override func didMoveToView(view: SKView) {
        
        backgroundColor = SKColor.whiteColor()
        let bgImage = SKSpriteNode(imageNamed:"gameplay_bg")
        bgImage.size = self.size
        bgImage.position = CGPointMake(self.size.width/2, self.size.height/2)
        addChild(bgImage)
        
        let topBar = SKSpriteNode(imageNamed:"top_bar")
        topBar.size = CGSize(width:self.size.width, height:(topBar.size.height/topBar.size.width)*self.size.width)
        topBar.position = CGPointMake(topBar.size.width/2, self.size.height-topBar.size.height/2)
        addChild(topBar)
        
        player.position = CGPoint(x: size.width * 0.5, y: player.size.height/2+15)
        player.zPosition = 1
        addChild(player)
        self.physicsBody = SKPhysicsBody (edgeLoopFromRect: self.frame)
        self.physicsWorld.gravity = CGVectorMake(0,0);
        self.physicsWorld.contactDelegate = self;
        
        score.horizontalAlignmentMode = .Left
        score.fontSize = 25;
        score.fontColor = UIColor.blackColor();
        score.text = NSString(format: "score:%i", GameCenter.sharedInstance.score)
        score.name = "score"
        score.position = CGPoint(x:0.0, y:size.height-score.frame.height)
        score.zPosition = 1
        addChild(score)
        
        life1.position = CGPoint(x:size.width-life1.frame.width*3, y:size.height-life1.frame.height)
        life2.position = CGPoint(x:size.width-life1.frame.width*2, y:size.height-life2.frame.height)
        life3.position = CGPoint(x:size.width-life1.frame.width*1, y:size.height-life3.frame.height)
        life1.zPosition = 1
        life2.zPosition = 1
        life3.zPosition = 1
        addChild(life1)
        addChild(life2)
        addChild(life3)
        
        playBackgroundMusic("Oh Shit by Dipp Dapp.mp3")

        runAction(SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.runBlock(createApp),
                SKAction.waitForDuration(1.0)
                ])
            ))
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        // 1 - Choose one of the touches to work with
        let touch = touches.anyObject() as UITouch
        let touchLocation = touch.locationInNode(self.scene)
        
        if(touchLocation.x > self.size.width/CGFloat(2.0)){
            player.moveToRight();
        }else{
            player.moveToLeft();
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        // 1 - Choose one of the touches to work with
        let touch = touches.anyObject() as UITouch
        let touchLocation = touch.locationInNode(self)
        
        player.stop();
        
        
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody?
        var secondBody: SKPhysicsBody?
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if let first = firstBody{
            // 2
            if(firstBody!.categoryBitMask == PhysicsCategory.App){
                if let second = secondBody{
                    if(secondBody!.categoryBitMask == PhysicsCategory.Player){
                        if let node = first.node{
                            if (node.isKindOfClass(Shitapp)){
                                runAction(SKAction.playSoundFileNamed("shit.mp3", waitForCompletion: false))
                            }else{
                                runAction(SKAction.playSoundFileNamed("app.mp3", waitForCompletion: false))
                            }
                            
                            appDidCollideWithPlayer(node as App)
                        }
                        
                    }
                }
                
            }
        
        }
        

    }
    
    func appDidCollideWithPlayer(app:App) {
        GameCenter.sharedInstance.score += app.score
        
        if(app.isKindOfClass(Shitapp)){
            switch GameCenter.sharedInstance.life
            {
            case 3:
                life3.removeFromParent()
                break
            case 2:
                life2.removeFromParent()
                break
            case 1:
                life1.removeFromParent()
                break
            default:
                break
            }
            GameCenter.sharedInstance.life--
            
            if(GameCenter.sharedInstance.life <= 0){
                gameOver()
            }
        }
        score.text = NSString(format: "score:%i", GameCenter.sharedInstance.score)
        app.removeFromParent()
        
    }
    
//    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
//        /* Called when a touch begins */
//    
//        }
//    }
//   
//    override func update(currentTime: CFTimeInterval) {
//        /* Called before each frame is rendered */
//    }
    
    func createApp(){
        let app:App = AppFactory.sharedInstance.createApp()
        addChild(app)
        app.move()
    }
    
    func gameOver(){
        backgroundMusicPlayer.stop()
        let reveal = SKTransition.fadeWithDuration(0.5)
        let gameOverScene = GameOverScene(size: self.size, won: false)
        self.view?.presentScene(gameOverScene, transition: reveal)
    }
}
