//
//  GameOverScene.swift
//  AlisonBirthday
//
//  Created by Poon Ka Hang on 4/2/2015.
//  Copyright (c) 2015年 Green Tomato. All rights reserved.
//

import SpriteKit
import AVFoundation

class GameOverScene: SKScene {
    var backgroundMusicPlayer: AVAudioPlayer!
    
    func playBackgroundMusic(filename: String) {
        let url = NSBundle.mainBundle().URLForResource(
            filename, withExtension: nil)
        if (url == nil) {
            println("Could not find file: \(filename)")
            return
        }
        
        var error: NSError? = nil
        backgroundMusicPlayer =
            AVAudioPlayer(contentsOfURL: url, error: &error)
        if backgroundMusicPlayer == nil {
            println("Could not create audio player: \(error!)")
            return
        }
        
        backgroundMusicPlayer.numberOfLoops = -1
        backgroundMusicPlayer.prepareToPlay()
        backgroundMusicPlayer.play()
    }
    
    init(size: CGSize, won:Bool) {
        
        super.init(size: size)
        
        backgroundColor = SKColor.whiteColor()
        //var message = won ? "You Won!" : "You Lose :["
        let bgImage = SKSpriteNode(imageNamed:"gameplay_bg")
        bgImage.size = self.size
        bgImage.position = CGPointMake(self.size.width/2, self.size.height/2)
        addChild(bgImage)
        
        
        var message = "Game Over"
        
        let label = SKLabelNode(fontNamed: "Chalkduster")
        label.text = message
        label.fontSize = 40
        label.fontColor = SKColor.blackColor()
        label.position = CGPoint(x: size.width/2, y: size.height/2 + label.frame.size.height * 1.5)
        addChild(label)
        
        let scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        scoreLabel.text = NSString(format:"\nScore:%i", GameCenter.sharedInstance.score);
        scoreLabel.fontSize = 40
        scoreLabel.fontColor = SKColor.blackColor()
        scoreLabel.position = CGPoint(x: size.width/2, y: size.height/2 )
        addChild(scoreLabel)
        
        let retry = SKLabelNode(fontNamed: "Chalkduster")
        retry.text = "Retry"
        retry.fontSize = 40
        retry.fontColor = SKColor.blackColor()
        retry.position = CGPoint(x: size.width/2, y: size.height/2 - retry.frame.size.height * 2)
        retry.name = "retry"
        addChild(retry)
        
        let card = SKSpriteNode(imageNamed: "bday_card")
        card.name = "card"
        card.zPosition = 2
        card.position = CGPoint(x: size.width/2, y: size.height/2)
        addChild(card)
        
        playBackgroundMusic("Happy_Birthday.mp3")
        
        //runAction(SKAction.playSoundFileNamed("pew-pew-lei.caf", waitForCompletion: false))
        
//        runAction(SKAction.sequence([
//            SKAction.waitForDuration(5.0),
//            SKAction.runBlock() {
//                GameCenter.sharedInstance.reset()
//                self.retry()
//            }
//            ]))
        
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        // 1 - Choose one of the touches to work with
        let touch = touches.anyObject() as UITouch
        let touchLocation = touch.locationInNode(self.scene)
        let node = self.nodeAtPoint(touchLocation)
        
        if (node.name == "card") {
            node.removeFromParent()
        }else if (node.name == "retry") {
            GameCenter.sharedInstance.reset()
            self.retry()
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func retry(){
        backgroundMusicPlayer.stop()
        let reveal = SKTransition.fadeWithDuration(0.5)
        let scene = GameScene(size: size)
        self.view?.presentScene(scene, transition:reveal)
    }
}